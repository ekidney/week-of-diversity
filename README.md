# Fedora Week of Diversity

Fedora Week of Diversity highlights and celebrates the diverse members of open source communities like Fedora and their contributions to their projects and community.

![FWD logo](https://gitlab.com/fedora/dei/week-of-diversity/uploads/c5b5a0ca78d7886bcc26e1715f91f7e6/FWD_logo.png)

## What is this place?
The Fedora DEI Team uses this repository to record ongoing work and to track issues about Fedora Week of Diversity.

# About Fedora Week of Diversity 2024

**When:** June 17-22, 2024. 
The online event will take place on June 21-22, and on the other days, we will publish interviews with our community members.

**Where:** Virtual event will happen on Matrix (link TBA)

**Team:**

* Project management: @jonatoni & @jwflory
* Event production: @jonatoni 
* Speaker management: @amsharma3 (lead)
* Content and promotion: @chrisidoko0017 (lead), @josephgayoso (lead), @obazeeconsole, @kanyin
* Design: @ekidney (lead), @chrisidoko0017, @obazeeconsole

## How to contact the team

Other than this repository, you can also find us on the DEI team Matrix room:

* [Matrix](https://matrix.to/#/#dei:fedoraproject.org)

